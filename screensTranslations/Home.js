module.exports =  [
/*0*/
{
    de: "Suche",
    en: "Search",
    es: "Búsqueda",
    fr: "Chercher",
    it: "Ricerca",
    nl: "Zoekopdracht",
    no: "Søk",
    pt: "Procurar",

},

/*0*/
{
    de: "Spielen",
    en: "Play",
    es: "Tocar",
    fr: "Jouer",
    it: "Giocare a",
    nl: "Toneelstuk",
    no: "Spille",
    pt: "Toque",

},

/*1*/
{
    de: "Sieg",
    en: "Win",
    es: "Victoria",
    fr: "Gagner",
    it: "Vincita",
    nl: "Winnen",
    no: "Vinne",
    pt: "Ganhar",

},
];