import { combineReducers } from 'redux';
import accountReducer from './accountReducer';
import productReducer from './productReducer';
import masterReducer from './masterReducer';

export default combineReducers({
    accountReducer,
    productReducer,
    masterReducer
});