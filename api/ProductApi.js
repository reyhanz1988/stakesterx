import {BASE_URL} from "@env"

let ProductApi = {
    getGames(vars) {
        let params = {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        };
        return fetch(BASE_URL + '/api/gamesPrizes?searchText='+vars.searchText+'&page='+vars.currentPage+'&offset='+vars.perPage, params)
            .then((res) => res.json())
            .catch((error) => {
                return 'error';
                console.warn("getGames => " + error);
            });
    },
};

module.exports = ProductApi;
