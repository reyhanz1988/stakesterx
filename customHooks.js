import React, {useEffect, useRef} from 'react';

export function usePrevious(value){
    const ref = useRef();
    useEffect(() => {
        ref.current = value;
    }, [value]);
    return ref.current;
}

export const useUpdateEffect = (callback, dependencies) => {
    const isFirstRender = useRef(true);
    useEffect(() => {
        if (isFirstRender.current) {
            isFirstRender.current = false;
        } 
        else {
            return callback();
        }
    }, dependencies);
}