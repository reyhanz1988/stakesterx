import React, {createContext, useEffect, useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { LogBox, Platform, StatusBar, StyleSheet, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import AppNav from './navigation/AppNav';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { Provider } from 'react-redux';
import store from './store';
import Orientation from 'react-native-orientation-locker';
import {FIRST_COLOR, SECOND_COLOR} from "@env";

export const LangContext = createContext('en');

const App = () => {
    const theme = {
        ...DefaultTheme,
        roundness: 5,
        colors: {
            ...DefaultTheme.colors,
            primary: FIRST_COLOR,
            accent: SECOND_COLOR,
        },
    };

    const styles = StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: '#fff',
        },
    });

    const [rotationLock, setRotationLock] = useState('');
    const [currentLang, setCurrentLang]   = useState('en');
    useEffect(()=>{
        LogBox.ignoreLogs(['Warning: ...']);
        LogBox.ignoreAllLogs();
        Orientation.getAutoRotateState((rotationLock) => setRotationLock(rotationLock));
        Orientation.lockToPortrait();
        AsyncStorage.getItem("LANG").then(value => {
            if(value == null){
                AsyncStorage.setItem('LANG', 'en').then((value) => {
                    setCurrentLang('en');
                });
            }
            else{
                setCurrentLang(value);
            }
        });
    },[]);

    return(
        <Provider store={store}>
            <View style={styles.container}>
                <LangContext.Provider value={{currentLang, setCurrentLang}}>
                    {Platform.OS === 'ios' && <StatusBar />}
                    <NavigationContainer>
                        <PaperProvider theme={theme}>
                            <AppNav />
                        </PaperProvider>
                    </NavigationContainer>
                </LangContext.Provider>
            </View>
        </Provider>
    );
}
export default App;