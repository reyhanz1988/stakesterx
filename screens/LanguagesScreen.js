import React, {useContext, useEffect, useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { LangContext } from "../App";
import {usePrevious, useUpdateEffect} from "../customHooks";
import { connect } from 'react-redux';
import * as masterActions from '../actions/masterActions';
import { StyleSheet, ScrollView, View } from 'react-native';
import { Appbar, Icon, List } from 'react-native-paper';
import CountryFlag from "react-native-country-flag";
import languages from '../screensTranslations/Languages';
import Loading from '../components/Loading';
//import Reload from '../components/Reload';
import {FIRST_COLOR} from "@env";

const LanguagesScreen = (props) => {
    const {currentLang}                     = useContext(LangContext);
    const {setCurrentLang}                  = useContext(LangContext);
    const [isLoading, setIsLoading]         = useState(true);
    const [refresh, setRefresh]             = useState(false);
    const [getDataError, setGetDataError]   = useState([]);
    const [getLang, setGetLang]             = useState([]);
    const prevGetLangRes                    = usePrevious(props.getLangRes);
    //FIRST LOAD
    useEffect(() => {
        props.getLang();
    }, []);
    //PROPS UPDATE
    useUpdateEffect(() => {
        if(JSON.stringify(prevGetLangRes) == JSON.stringify(props.getLangRes)){
            setGetLang(props.getLangRes);
            setRefresh(false);
            setIsLoading(false);
        }
        else{
            if(props.getLangRes == 'error'){
                setGetDataError(props.getLangRes);
                setIsLoading(false);
                setRefresh(false);
            }
            else{
                if(props.getLangRes.status_msg){
                    setGetDataError(props.getLangRes.status_msg);
                    setIsLoading(false);
                    setRefresh(false);
                }
                else{
                    if(props.getLangRes.length > 0){
                        setGetLang(props.getLangRes);
                        setGetDataError('');
                        setIsLoading(false);
                    }
                    else{
                        setIsLoading(false);
                    }
                }
            }
        }
    }, [props.getLangRes,getLang,refresh]);
    //GET DATA
    const getData = () => {
        setRefresh(true);
        props.getLang(vars);
    }
    //SAVE FORM
    const saveForm = (lang) => {
        AsyncStorage.setItem('LANG', lang).then((value) => {
            setCurrentLang(lang);
            setIsLoading(false)
        });
    }
    //RENDER
    if(isLoading == true){
        return (
            <Loading />
        );
    }
    else{
        let listViews = [];
        for(let i=0;i<getLang.length;i++){
            let bgColor;
            if(getLang[i].language_iso == currentLang){
                bgColor = "#FADA5E";
            }
            else{
                bgColor = "#fff";
            }
            let cfIcon = getLang[i].language_iso;
            if(getLang[i].language_iso == 'en'){
                cfIcon = 'gb';
            }
            listViews.push(
                <List.Item
                    key={i}
                    title={getLang[i].language}
                    titleStyle={{marginLeft:20}}
                    left={props => <CountryFlag style={{marginTop:8}} isoCode={cfIcon} size={24} />}
                    right={props => <List.Icon {...props} icon='chevron-right' />}
                    onPress={() => saveForm(getLang[i].language_iso)}
                    style={{borderBottomWidth:1,borderBottomColor:'#eee',backgroundColor:bgColor}}
                />
            );
        }
        return (
            <View style={styles.wrapper}>
                <Appbar.Header style={{backgroundColor:FIRST_COLOR}}>
                    <Appbar.BackAction style={{marginTop:10}} color="#fff" onPress={() => props.navigation.goBack()} />
                    <Appbar.Content titleStyle={{color:'#fff',textAlign:'center'}} title={languages[0][currentLang]} />
                </Appbar.Header>
                <ScrollView>
                    {listViews}
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        width:'100%',
        height:'100%'
    },
});
function mapStateToProps(state, props) {
    return {
        getLangRes: state.masterReducer.getLangRes
    }
}
const mapDispatchToProps = {
    ...masterActions
};
export default connect(mapStateToProps, mapDispatchToProps)(LanguagesScreen);