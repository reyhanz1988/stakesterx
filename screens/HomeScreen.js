import React, {useContext, useEffect, useState} from 'react';
import { LangContext } from "../App";
import {usePrevious, useUpdateEffect} from "../customHooks";
import { ActivityIndicator, Dimensions, Image, RefreshControl, ScrollView, StyleSheet, Text, View } from 'react-native';
import { Appbar, Button, Card, List, Title, Paragraph, Searchbar } from 'react-native-paper';
import logo from '../assets/images/logo.png';
import { connect } from 'react-redux';
import * as productActions from '../actions/productActions';
import Loading from '../components/Loading';
import languages from '../screensTranslations/Home';
import {FIRST_COLOR, SECOND_COLOR} from "@env";

const HomeScreen = (props) => {
    
    const {currentLang}                     = useContext(LangContext);
    const [isLoading, setIsLoading]         = useState(true);
    const [refresh, setRefresh]             = useState(false);
    const [moreLoading, setMoreLoading]     = useState(false);
    const [moreState, setMoreState]         = useState(true);
    const [getDataError, setGetDataError]   = useState([]);
    const [getGames, setGetGames]           = useState([]);
    const prevGetGamesRes                   = usePrevious(props.getGamesRes);
    const [searchState, setSearchState]     = useState(false);
    const [typingTimeout, setTypingTimeout] = useState(false);
    const [searchText, setSearchText]       = useState('');
    const [currentPage, setCurrentPage]     = useState(1);
    const [perPage, setPerPage]             = useState(10);

    const vars = {};
    vars.currentLang = currentLang;
    vars.searchText = searchText;
    vars.currentPage = currentPage;
    vars.perPage = perPage;
    
    //FIRST LOAD
    useEffect(() => {
        props.getGames(vars);
    }, []);
    //PROPS UPDATE
    useUpdateEffect(() => {
        if(JSON.stringify(prevGetGamesRes) == JSON.stringify(props.getGamesRes)){
            setGetGames(props.getGamesRes);
            setRefresh(false);
            setIsLoading(false);
            setSearchState(false);
            setMoreLoading(false);
        }
        else{
            if(props.getGamesRes == 'error'){
                setGetDataError(props.getGamesRes);
                setIsLoading(false);
                setRefresh(false);
            }
            else{
                if(props.getGamesRes.status_msg){
                    setGetDataError(props.getGamesRes.status_msg);
                    setIsLoading(false);
                    setRefresh(false);
                }
                else{
                    if(props.getGamesRes.length > 0){
                        if(currentPage == 1){
                            setGetGames(props.getGamesRes);
                            setGetDataError('');
                            setIsLoading(false);
                            setSearchState(false);
                        }
                        else{
                            let concatData = [...getGames, ...props.getGamesRes];
                            setGetGames(concatData);
                            setGetDataError('');
                        }
                    }
                    else{
                        setIsLoading(false);
                        setMoreState(false);
                    }
                }
            }
        }
        if(searchState == true && (searchText.length == 0 || searchText.length >= 3)){
            setTypingTimeout(
                setTimeout(() => {
                    getData();
                }, 500)
            );
        }
    }, [props.getGamesRes,getGames,searchState,refresh]);
    //GET DATA
    const getData = () => {
        setCurrentPage(1);
        setRefresh(true);
        props.getGames(vars);
    }
    //GET MORE DATA
    const getMoreData = () => {
        if(moreLoading == false && moreState == true){
            setCurrentPage(currentPage+1);
            setMoreLoading(true);
            props.getGames(vars);
        }
    }
    //SEARCH
    const searchGames = searchInput => {
        if (typingTimeout) {
            clearTimeout(typingTimeout);
        }
        setSearchText(searchInput);
        setSearchState(true);
    };
    //RENDER
    if(isLoading == true){
        return (
            <Loading />
        );
    }
    else{
        let data = getGames;
        let gamesView = [];
        let prizesView = [];
        if(data.length > 0){
            for(let i=0; i<data.length; i++){
                if(data[i]['prizes'].length > 0){
                    prizesView[i] = [];
                    for(let j=0; j<data[i]['prizes'].length; j++){
                        prizesView[i].push(
                            <List.Item
                                key={'prize_'+i+'_'+j}
                                title={data[i]['prizes'][j].diamond+' + '+languages[2][currentLang]+' $'+data[i]['prizes'][j].prize}
                                titleStyle={{marginLeft:-32,fontSize:14}}
                                left={props => <List.Icon {...props} color={'#005d67'} icon={'diamond-stone'} />}
                                right=  {
                                            props => <Button icon="gamepad" mode="contained" onPress={() => console.log('Pressed')} labelStyle={{color:SECOND_COLOR}}>
                                                        {languages[1][currentLang]}
                                                    </Button>
                                        }
                                style={{borderBottomWidth:1,borderBottomColor:'#ddd'}}
                            />
                        );
                    }
                }
                gamesView.push(
                    <Card key={'games_'+i} style={{marginBottom:20,marginLeft:20,marginRight:20}}>
                        <Card.Cover source={{ uri: 'https://smokeapps.com/assets/gfx/games/'+(data[i].src_image).replace(".", "_400.") }} />
                        <Card.Content>
                            <Title style={{position:'absolute',marginLeft:10,marginTop:-40}}>
                                <View style={{backgroundColor:'rgba(0, 0, 0, 0.7)',padding:10,borderRadius:5}}>
                                    <Text style={{color:'#fff'}}>{data[i].game}</Text>
                                </View>
                            </Title>
                            <View>
                                {prizesView[i]}
                            </View>
                        </Card.Content>
                    </Card>
                );
            }
        }
        let bottomLoading;
        if(moreLoading == true){
            bottomLoading = (
                <ActivityIndicator />
            );
        }
        return (
            <View style={styles.wrapper}>
                <Appbar.Header style={{backgroundColor:SECOND_COLOR}}>
                    {/*<Appbar.Content titleStyle={{color:'#fff',textAlign:'center'}} title={languages[0][currentLang]} />*/}
                    <Image source={logo} style={{resizeMode:'contain',width:Dimensions.get('window').width,height:40}} />
                </Appbar.Header>
                <Searchbar
                    placeholder={languages[0][currentLang]}
                    onChangeText={searchGames}
                    value={searchText}
                    style={{margin:15}}
                />
                <ScrollView
                    onScroll={(e) => {
                        let paddingToBottom = 100;
                        paddingToBottom += e.nativeEvent.layoutMeasurement.height;
                        if((e.nativeEvent.contentOffset.y >= e.nativeEvent.contentSize.height - paddingToBottom)) {
                            getMoreData();
                        }
                    }}
                    refreshControl={<RefreshControl refreshing={refresh} onRefresh={()=>getData()} />}
                >
                    {gamesView}
                    {bottomLoading}
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    }
});

function mapStateToProps(state, props) {
    return {
        getGamesRes: state.productReducer.getGamesRes,
    }
}
const mapDispatchToProps = {
    ...productActions
};
export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);